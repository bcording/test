FROM alpine:latest

RUN wget http://gitea.docker/homelab/udp-docker/raw/branch/master/multirun -O /bin/multirun && \
    wget http://gitea.docker/homelab/udp-docker/raw/branch/master/udp-broadcast-relay-redux -O /bin/udp-broadcast-relay-redux && \
    wget http://gitea.docker/homelab/udp-docker/raw/branch/master/udp-daikin.sh -O /bin/udp-daikin.sh && \
    wget http://gitea.docker/homelab/udp-docker/raw/branch/master/udp-mdns.sh -O /bin/udp-mdns.sh && \
    wget http://gitea.docker/homelab/udp-docker/raw/branch/master/udp-ssdp.sh -O /bin/udp-ssdp.sh && \
    chmod +x /bin/udp-* && \
    chmod +x /bin/multirun
CMD multirun /bin/udp-daikin.sh /bin/udp-mdns.sh /bin/udp-ssdp.sh